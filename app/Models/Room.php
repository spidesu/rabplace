<?php

namespace App\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
class Room extends Model
{
    use SoftDeletes;
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * Прячем id docker-контейнера от лишних глаз
     *
     * @var array
     */
    protected $hidden =[
        'container_id', 'pivot','owner_id', 'updated_at'
    ];

    protected $fillable =[
        'title','description','owner_id', 'container_id', 'created_at', 'updated_at',
    ];

    /**
     * Получаем владельца (создателя) комнаты
     *
     * @return BelongsTo
     */
    public function owner() {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * Получаем список участников комнаты
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function participants() {
        return $this->belongsToMany(User::class, 'room_users');
    }

}
