<?php

namespace App\Jobs;

use App\Events\PackageUploaded;
use App\Repositories\RoomRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ZipArchive;

class ProcessPackageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Request
     */
    protected $packageId;
    protected $path;
    protected $roomId;
    protected $roomRepository;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($packageId, $path, $roomId)
    {
        $this->packageId = $packageId;
        $this->path = $path;
        $this->roomId = $roomId;
        $this->roomRepository = app(RoomRepository::class);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $storagePath = storage_path('packages/');
        $zip = new ZipArchive;
        $res = $zip->open($storagePath.$this->path);
        if (!$res === TRUE) {
            logs()->info('Пакет не может быть открыт '.$this->path);
            return;
        }
        $zip->extractTo($storagePath.$this->packageId);
        $zip->close();

        Storage::disk('packages')->delete($this->path);
        logs()->info('Пакет загружен '.$this->path);

        $room = $this->roomRepository->update($this->roomId,[
            'package_id' => $this->packageId
        ]);
        event(new PackageUploaded($room->id,$room->package_id));
    }
}
