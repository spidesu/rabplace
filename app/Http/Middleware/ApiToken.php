<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class ApiToken
{
    /**
     * Мидлвар для проверки наличия токена
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($token = $request->bearerToken()) {

            $user = User::where('api_token', $token)->toBase()->first('id');
            if ($user) {
            return $next($request);
                }
            else {
                return response()->json([
                    'msg' => "Provided token isn't assigned to anyone"
                ],422);
            }
        }
        else {
            return response()->json([
                'msg' => 'No API Token presented'
            ],403);
        }
    }
}
