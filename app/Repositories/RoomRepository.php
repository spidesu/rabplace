<?php


namespace App\Repositories;

use App\Jobs\ProcessPackageJob;
use App\Models\Room as Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class RoomRepository extends BaseRepository
{
    protected $model;

    /**
     * У каждого репозитория должен быть метод getModelClass, чтобы понимать, с какой моделью мы работаем
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Информация о комнате
     *
     * @param int $id
     * @return mixed
     */
    public function getRoomInfo($id) {
        $columns = [
            'id', 'title', 'description', 'owner_id'
        ];
        $room = $this->startConditions()
            ->with('owner:id,display_name,url_avatar', 'participants:user_id as id,display_name,url_avatar')
            ->find($id);
        return $room;
    }

    public function getRoom($id) {
        $room = $this->startConditions()
            ->find($id);
    }

    public function getPublicRoomsList() {
        $columns = [
            'id', 'title', 'description', 'owner_id'
        ];
        $rooms = $this->startConditions()
            ->where('password', NULL)
            ->with('owner:id,display_name,url_avatar', 'participants:user_id as id,display_name,url_avatar')
            ->get();

        return $rooms;
    }
    /**
     *
     * Изменить комнату
     *
     * @param $id
     * @return mixed
     */
    public function update($id, $data) {
        $this->startConditions()
            ->where('id', $id)
            ->update($data);

        $room = $this->startConditions()
            ->where('id', $id)
            ->first();
        return $room;
    }
    /**
     * Id комнаты и краткая информация об её участниках (id,login, avatar)
     *
     * @param int $id
     * @return mixed
     */
    public function getParticipants($id) {

        $columns = [
            'id'
        ];
        $participants = $this->startConditions()
            ->where('id', $id)
            ->with('participants:user_id as id,login,url_avatar')
            ->first($columns);
        return $participants;
    }

    public function join($roomId, $userId) {
        $room = $this->getRoom($roomId);
        $room->participants()
            ->syncWithoutDetaching($userId);
        return($room->fresh());

    }

    public function leave($roomId, $userId) {
        $room = $this->getRoom($roomId);
        $room->participants()->detach($userId);
        $rooms = $this->getPublicRoomsList();
        return $rooms;
    }

    public function kick($roomId, $userId) {
        $room = $this->getRoom($roomId);
        $room->participants()->detach($userId);
    }

    public function ban($roomId, $userId) {
        $this->kick($roomId, $userId);

    }

    public function create($data) {

        if (!isset($data['owner_id'])) {
            $data['owner_id'] = Auth::id();
        }
        $room = $this->startConditions()
            ->create($data);
        return $room;
        }




}
