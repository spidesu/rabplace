<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoomRequest;
use App\Http\Resources\ParticipantResource;
use App\Http\Resources\RoomResource;
use App\Jobs\ProcessPackageJob;
use App\Repositories\RoomRepository;
use App\Storages\PackageStorage;
use Illuminate\Http\Request;
use App\Models\Room;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RoomController extends Controller
{

    protected $roomRepository;

    public function __construct()
    {
        parent::__construct();
        $this->roomRepository = app(RoomRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoomRequest $request
     * @return RoomResource
     */
    public function store(RoomRequest $request)
    {
        $data = $request->only('title','description');
        $room = $this->roomRepository->create($data);

        return new RoomResource($room);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\ParticipantResourceCollection
     */
    public function participants($id, Request $request) {
        //dd($request);
        $participants = $this->roomRepository->getParticipants($id);

        return ParticipantResource::collection($participants);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return RoomResource
     */
    public function show($id,Request $request)
    {
        $room = $this->roomRepository->getRoomInfo($id);
        return new RoomResource($room);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoomRequest $request
     * @param int $id
     * @return RoomResource
     */
    public function update($id,RoomRequest $request)
    {
        $data = $request->input();
        $room = $this->roomRepository->update($id, $data);

        return (new RoomResource($room));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function join(Request $request)
    {
        $room = $this->roomRepository->join($request->room_id, Auth::id());
        return (new RoomResource($room));
    }

    public  function leave(Request $request)
    {
        $room = $this->roomRepository->leave($request->room_id, Auth::id());
        return RoomResource::collection($room);
    }

    public function uploadPackage(Request $request) {
        $roomId = $request->room_id;
        $packageId = Str::random(30);
        Storage::makeDirectory($packageId);
        $path = $request->file('package')->store($packageId,'packages');
        ProcessPackageJob::dispatch($packageId, $path, $roomId);

    }
}
