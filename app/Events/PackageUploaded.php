<?php

namespace App\Events;

use App\Models\Room;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PackageUploaded extends BaseGameEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    /**
     * @var Room
     */
    public $room_id;
    public $package_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($room_id, $package_id)
    {
        $this->room_id = $room_id;
        $this->package_id = $package_id;
    }



    public function broadcastAs()
    {
        return 'package';
    }
}
