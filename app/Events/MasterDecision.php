<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class MasterDecision extends BaseGameEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $room_id;
    public $decision;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($room_id,$decision)
    {
        $this->room_id = $room_id;
        $this->decision = $decision;
    }


    public function broadcastAs()
    {
        return 'master';
    }
}

