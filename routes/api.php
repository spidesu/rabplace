<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');
    Route::get('register/activate/{token}','Auth\RegisterController@registerActivate');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('logout', 'Auth\LoginController@logout');
        Route::get('user', 'Auth\LoginController@user');
        Route::resource('room','RoomController');
        Route::resource('user','UserController');
    });
});

Route::group ([
    'prefix' => 'room'
], function () {
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::post('join', 'RoomController@join');
        Route::post('leave', 'RoomController@leave');
        Route::post('{room_id}/upload', 'RoomController@uploadPackage');
    });
});

